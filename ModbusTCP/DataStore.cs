﻿using System;

namespace ModbusTCP
{
    public class DataStore
    {
        public byte UnitId;

        private bool[] _DiscreteInputs;
        private bool[] _Coils;
        private UInt16[] _InputRegisters;
        private UInt16[] _HoldingRegisters;

        public bool[] DiscreteInputs { get; }
        public bool[] Coils { get; }
        public UInt16[] InputRegisters { get; }
        public UInt16[] HoldingRegisters { get { return _HoldingRegisters; } }

        public delegate void DataStoreUpdateEventHandler(DataStore sender, int firstItem, int length);
        public event DataStoreUpdateEventHandler DiscreteInputsUpdateEvent;
        public event DataStoreUpdateEventHandler CoilsUpdateEvent;
        public event DataStoreUpdateEventHandler InputRegistersUpdateEvent;
        public event DataStoreUpdateEventHandler HoldingRegistersUpdateEvent;
              
        public DataStore(byte UnitId)
        {
            this.UnitId = UnitId;
            _DiscreteInputs = new bool[65535];
            _Coils = new bool[65535];
            _InputRegisters = new UInt16[65535];
            _HoldingRegisters = new UInt16[65535];
        }

        public void updateDiscreteInputs(bool[] sourceArray, int destinationIndex)
        {
            Array.Copy(sourceArray, 0, _DiscreteInputs, destinationIndex, sourceArray.Length);
            DiscreteInputsUpdateEvent?.Invoke(this, destinationIndex, sourceArray.Length);
        }
        public void updateDiscreteInputs(bool[] sourceArray, int sourceIndex, int destinationIndex, int length)
        {
            Array.Copy(sourceArray, sourceIndex, _DiscreteInputs, destinationIndex, length);
            DiscreteInputsUpdateEvent?.Invoke(this, destinationIndex, length);
        }
        public void updateCoils(bool[] sourceArray, int destinationIndex)
        {
            Array.Copy(sourceArray, 0, _Coils, destinationIndex, sourceArray.Length);
            CoilsUpdateEvent?.Invoke(this, destinationIndex, sourceArray.Length);
        }
        public void updateCoils(bool[] sourceArray, int sourceIndex, int destinationIndex, int length)
        {
            Array.Copy(sourceArray, sourceIndex, _Coils, destinationIndex, length);
            CoilsUpdateEvent?.Invoke(this, destinationIndex, length);
        }
        public void updateInputRegisters(UInt16[] sourceArray, int destinationIndex)
        {
            Array.Copy(sourceArray, 0, _InputRegisters, destinationIndex, sourceArray.Length);
            InputRegistersUpdateEvent?.Invoke(this, destinationIndex, sourceArray.Length);
        }
        public void updateInputRegisters(UInt16[] sourceArray, int sourceIndex, int destinationIndex, int length)
        {
            Array.Copy(sourceArray, sourceIndex, _InputRegisters, destinationIndex, length);
            InputRegistersUpdateEvent?.Invoke(this, destinationIndex, length);
        }
        public void updateHoldingRegisters(UInt16[] sourceArray, int destinationIndex)
        {
            Array.Copy(sourceArray, 0, _HoldingRegisters, destinationIndex, sourceArray.Length);
            HoldingRegistersUpdateEvent?.Invoke(this, destinationIndex, sourceArray.Length);
        }
        public void updateHoldingRegisters(UInt16[] sourceArray, int sourceIndex, int destinationIndex, int length)
        {
            Array.Copy(sourceArray, sourceIndex, _HoldingRegisters, destinationIndex, length);
            HoldingRegistersUpdateEvent?.Invoke(this, destinationIndex, length);
        }
    }
}
