﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ModbusTCP
{
    public class SlaveNetwork
    {        
        public Dictionary<byte, Slave> slaves;        
        private TcpListener tcpListener;
        private static ArrayList clientSockets;

        public SlaveNetwork()
        {
            slaves = new Dictionary<byte, Slave>();
            clientSockets = new ArrayList();

            StartServer();
        }

        ~SlaveNetwork()
        {
            foreach(ClientHandler ch in clientSockets)
            {
                ch.Close();
            }
            tcpListener.Stop();
        }

        private void StartServer()
        {
            try
            {
                Console.WriteLine("Server starting...");
                tcpListener = new TcpListener(IPAddress.Parse("0.0.0.0"), 502);
                tcpListener.Start();
                tcpListener.BeginAcceptTcpClient(ConnectionHandler, null);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void ConnectionHandler(IAsyncResult result)
        {
            TcpClient client = null;

            try
            {
                client = tcpListener.EndAcceptTcpClient(result);
            }
            catch (System.Exception)
            {
                return;
            }

            tcpListener.BeginAcceptTcpClient(ConnectionHandler, null);

            if (client != null)
            {
                lock (clientSockets.SyncRoot)
                {
                    clientSockets.Add(new ClientHandler(client, this));
                }                
            }
        }       

        public bool AddSlave(byte unitId)
        {
            try
            {
                slaves.Add(unitId, new Slave(unitId));
                return true;
            }
            catch(ArgumentException)
            {
                return false;
            }
        }

        public bool AddSlave(Slave slave)
        {
            try
            {
                slaves.Add(slave.UnitId, slave);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        public void RemoveSlave(byte unitId)
        {
            slaves.Remove(unitId);
        }

        public byte[] ComputeFrame(byte[] modbusTCP_ADU)
        {
            Request req = new Request(modbusTCP_ADU);
            Response res;

            if(req.unitId != 0 && !slaves.Keys.Contains(req.unitId))
            {
                Exception exceptionResponse = new Exception(req, Exception.GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND);
                return exceptionResponse.GetFrame();
            }

            if (req.unitId != 0)
            {
                switch (req.functionCode)
                {
                    case 3: //Read Multiple Registers (hexa : 03)
                        return slaves[req.unitId].ReadMultipleRegisters(req);
                    case 6: //WriteSingle Register (hexa : 06)
                        return slaves[req.unitId].WriteMultipleRegisters(req);
                    case 16: //WriteMultipleRegisters (hexa : 10)
                        return slaves[req.unitId].WriteMultipleRegisters(req);
                    default:
                        Exception exceptionResponse = new Exception(req, Exception.ILLEGAL_FUNCTION_CODE);
                        return exceptionResponse.GetFrame();
                }
            }
            else
            {
                //TODO : broadcast
            }

            return null;
        }
    }
}
