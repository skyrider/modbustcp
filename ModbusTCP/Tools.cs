﻿using System;
using System.Linq;

namespace ModbusTCP
{
    public class Tools
    {
        public static byte GetRegByteHI(UInt16 reg)
        {
            byte[] bytes = BitConverter.GetBytes(reg);
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
            return bytes[0];
        }

        public static byte GetRegByteLO(UInt16 reg)
        {
            byte[] bytes = BitConverter.GetBytes(reg);
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
            return bytes[1];
        }

        public static byte[] GetRegBytes(UInt16 reg)
        {
            byte[] bytes = BitConverter.GetBytes(reg);
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
            return bytes;
        }

        public static UInt16 GetRegister(byte[] bytes, int index = 0, bool isLittleEndian = false)
        {
            if (BitConverter.IsLittleEndian != isLittleEndian)
            {
                return BitConverter.ToUInt16(new byte[2] { bytes[index + 1], bytes[index] }, 0);
            }
            else
            {
                return BitConverter.ToUInt16(bytes, index);
            }
        }        
    }
}
