﻿using System;

namespace ModbusTCP
{
    public class Response
    {
        public UInt16 transactionId;
        public UInt16 protocolId;
        public UInt16 nbBytes;
        public byte unitId;
        public byte functionCode;
        public byte[] data;

        public Response(byte[] reqBytes) 
        {
            transactionId = Tools.GetRegister(reqBytes, 0);
            protocolId = Tools.GetRegister(reqBytes, 2);
            nbBytes = Tools.GetRegister(reqBytes, 4);

            unitId = reqBytes[6];            
            functionCode = reqBytes[7];
            data = new byte[nbBytes - 2];
            Array.Copy(reqBytes, 8, data, 0, nbBytes - 2);
        }

        public Response(UInt16 transactionId, UInt16 protocolId, byte unitId, byte functionCode, byte[] data)
        {
            this.transactionId = transactionId;
            this.protocolId = protocolId;
            this.unitId = unitId;
            this.functionCode = functionCode;
            this.data = data;
            nbBytes = (UInt16)(data.Length + 2);
        }

        public Response(Request req, byte[] data)
        {
            transactionId = req.transactionId;
            protocolId = req.protocolId;
            unitId = req.unitId;
            functionCode = req.functionCode;
            this.data = data;
            nbBytes = (UInt16)(data.Length + 2);
        }

        public byte[] GetFrame()
        {
            nbBytes = (UInt16)(data.Length + 2);

            byte[] bytes = new byte[6 + nbBytes];
            bytes[0] = Tools.GetRegByteHI(transactionId);
            bytes[1] = Tools.GetRegByteLO(transactionId);
            bytes[2] = Tools.GetRegByteHI(protocolId);
            bytes[3] = Tools.GetRegByteLO(protocolId);
            bytes[4] = Tools.GetRegByteHI(nbBytes);
            bytes[5] = Tools.GetRegByteLO(nbBytes);
            bytes[6] = unitId;
            bytes[7] = functionCode;
            Array.Copy(data, 0, bytes, 8, data.Length);
            return bytes;
        }
    }
}
