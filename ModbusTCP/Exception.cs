﻿using System;

namespace ModbusTCP
{
    public class Exception
    {
        public UInt16 transactionId;
        public UInt16 protocolId;
        public UInt16 nbBytes;
        public byte unitId;
        public byte functionCode;
        public byte exceptionCode;

        public Exception(UInt16 transactionId, UInt16 protocolId, byte unitId, byte functionCode, byte exceptionCode) 
        {
            this.transactionId = transactionId;
            this.protocolId = protocolId;
            this.nbBytes = 3;
            this.unitId = unitId;
            this.functionCode = functionCode;
            this.exceptionCode = exceptionCode;
        }

        public Exception(Request req, byte exceptionCode)
        {
            this.transactionId = req.transactionId;
            this.protocolId = req.protocolId;
            this.nbBytes = 3;
            this.unitId = req.unitId;
            this.functionCode = req.functionCode;
            this.exceptionCode = exceptionCode;
        }

        public Exception(Byte[] reqBytes)
        {
            transactionId = Tools.GetRegister(reqBytes, 0);
            protocolId = Tools.GetRegister(reqBytes, 2);
            unitId = reqBytes[6];
            functionCode = reqBytes[7];
            exceptionCode = reqBytes[8];
        }
        public byte[] GetFrame()
        {
            byte[] bytes = new byte[9];
            bytes[0] = Tools.GetRegByteHI(transactionId);
            bytes[1] = Tools.GetRegByteLO(transactionId);
            bytes[2] = Tools.GetRegByteHI(protocolId);
            bytes[3] = Tools.GetRegByteLO(protocolId);
            bytes[4] = Tools.GetRegByteHI(3);
            bytes[5] = Tools.GetRegByteLO(3);
            bytes[6] = unitId;
            bytes[7] = (byte)(functionCode | 128); //Bit 8 = 1 pour signaler l'erreur
            bytes[8] = exceptionCode;
            return bytes;
        }

        public const byte ILLEGAL_FUNCTION_CODE = 1;
        public const byte ILLEGAL_DATA_ADDRESS = 2;
        public const byte ILLEGAL_DATA_VALUE = 3;
        public const byte SLAVE_DEVICE_FAILURE = 4;
        public const byte ACKNOWLEDGE = 5;
        public const byte SLAVE_DEVICE_BUSY = 6;
        public const byte NEGATIVE_ACKNOWLEDGE = 7;
        public const byte MEMORY_PARITY_ERROR = 8;
        public const byte GATEWAY_PATH_UNAVAILABLE = 10;
        public const byte GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND = 11;
    }
}
