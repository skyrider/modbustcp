﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace ModbusTCP
{
    internal class ClientHandler
    {        
        private TcpClient client;
        private SlaveNetwork sn;
        private IntPtr socketHandle;
        private bool run;

        internal ClientHandler(TcpClient client, SlaveNetwork sn)
        {
            this.client = client;
            this.sn = sn;
            socketHandle = client.Client.Handle;
            run = true;
            Thread thread = new Thread(ProcessConnection)
            {
                IsBackground = true
            };
            thread.Start();
        }

        private void ProcessConnection()
        {
            using (client)
            {
                byte[] frame = new byte[] { };
                NetworkStream netStream = client.GetStream();
                Console.WriteLine("Client Connected: " + socketHandle.ToString());

                while (run)
                {
                    if (client.Client.Poll(5000, SelectMode.SelectRead) && client.Client.Available == 0)
                    {
                        run = false;
                        if (client.Connected) client.Close();
                        Console.WriteLine("Client Disconnected: " + socketHandle.ToString());
                        break;
                    }
                    if (netStream.DataAvailable)
                    {
                        if (client.Available > 0)
                        {
                            byte[] buff = new byte[client.Available];
                            netStream.Read(buff, 0, client.Available);
                            byte[] res = sn.ComputeFrame(buff);
                            netStream.Write(res, 0, res.Length);
                        }
                    }
                }
            }
        }

        public void Close()
        {
            run = false;
            if(client.Connected) client.Close();
            Console.WriteLine("Client Disconnected: " + socketHandle.ToString());
        }
    }
}
