﻿using System;

namespace ModbusTCP
{
    public class Slave
    {
        public byte UnitId;
        public DataStore datastore;      

        public Slave(byte unitId)
        {
            this.UnitId = unitId;
            datastore = new DataStore(unitId);
        }

        public Slave(DataStore ds)
        {
            this.UnitId = ds.UnitId;
            datastore = ds;
        }

        public byte[] ReadMultipleRegisters(Request req)
        {
            UInt16 firstRegister = Tools.GetRegister(req.data, 0);
            UInt16 nbRegs = Tools.GetRegister(req.data, 2);
            byte byteCount = (byte)(nbRegs * 2);

            if (nbRegs < 1 || nbRegs > 123)
            {
                Exception exceptionResponse = new Exception(req, Exception.ILLEGAL_DATA_VALUE);
                return exceptionResponse.GetFrame();
            }

            if (firstRegister + nbRegs - 1 > datastore.HoldingRegisters.Length - 1)
            {
                Exception exceptionResponse = new Exception(req, Exception.ILLEGAL_DATA_ADDRESS);
                return exceptionResponse.GetFrame();
            }

            byte[] data = new byte[1 + nbRegs * 2];
            data[0] = byteCount;

            int dataIdx = 1;
            for (int i = firstRegister; i <= firstRegister + nbRegs - 1; i++)
            {
                data[dataIdx] = Tools.GetRegByteHI(datastore.HoldingRegisters[i]);
                data[dataIdx+1] = Tools.GetRegByteLO(datastore.HoldingRegisters[i]);
                dataIdx += 2;
            }

            Response res = new Response(req, data);
            return res.GetFrame();
        }

        public byte[] WriteMultipleRegisters(Request req)
        {
            UInt16 firstRegister = Tools.GetRegister(req.data, 0);
            UInt16 nbRegs = Tools.GetRegister(req.data, 2);
            byte byteCount = req.data[4];

            if(nbRegs < 1 || nbRegs > 123 || nbRegs*2 != byteCount || (req.data.Length-5) % 2 > 0)
            {
                Exception exceptionResponse = new Exception(req, Exception.ILLEGAL_DATA_VALUE);
                return exceptionResponse.GetFrame();
            }

            if(firstRegister + nbRegs - 1 > datastore.HoldingRegisters.Length - 1)
            {
                Exception exceptionResponse = new Exception(req, Exception.ILLEGAL_DATA_ADDRESS);
                return exceptionResponse.GetFrame();
            }
            
            int dataIdx = 5;
            UInt16[] registers = new UInt16[nbRegs];
            for(int i = 0; i < nbRegs; i++)
            {
                registers[i] = Tools.GetRegister(req.data, dataIdx);
                dataIdx += 2;
            }            
            datastore.updateHoldingRegisters(registers, firstRegister);

            byte[] data = new byte[4];
            Array.Copy(Tools.GetRegBytes(firstRegister), 0, data, 0, 2);
            Array.Copy(Tools.GetRegBytes(nbRegs), 0, data, 2, 2);
            Response res = new Response(req, data);
            return res.GetFrame();
        }
    }
}
