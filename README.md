# ModbusTCP

* ModbusTCP is a free software library to implementing the Modbus protocol.
* This library is written in C# and supports TCP only (Master and/or Slave(s)).

## Slave modbus functions supported

* 03 (0x03) Read Holding Registers
* 06 (0x06) Write Single Register
* 16 (0x10) Write Multiple registers

## Multi slaves example

Firstly, create a SlaveNetwork instance 

```C#
SlaveNetwork sn = new SlaveNetwork();
```

Create some slaves
```C#
Slave slave1 = new Slave(1); // 1 is the unit ID
Slave slave2 = new Slave(2); // 2 is the unit ID
```
Add slaves to network
```C#
sn.AddSlave(slave1);
sn.AddSlave(slave2);
```

That's all !

## License

This project is licensed under the GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details